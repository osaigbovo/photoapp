package com.xyz.photoapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.ImageView
import com.xyz.photoapp.models.Photo

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val imageView = findViewById<ImageView>(R.id.imageView)

        //Get the Photo Object that was passed into us.
        val photo = intent.getSerializableExtra(PHOTO) as Photo

        photo?.webformatURL.let {
            GlideApp.with(this)
                    .load(photo?.webformatURL)
                    .into(imageView)
        }

        imageView.setOnClickListener {
            finish()
        }
    }

    // Companion Object is Kotlin's way of creating CONSTANTS & STATICS
    companion object {
        val PHOTO = "PHOTO"
    }
}
