package com.xyz.photoapp.api

import com.xyz.photoapp.models.PhotoList
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PhotoRetriever {

    private val service: PhotoAPI

    // Initialisation method for creating the service.
    // Kotlin uses the init code block for constructor code.
    init {
        val retrofit = Retrofit.Builder()
                .baseUrl("http://pixabay.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        service = retrofit.create(PhotoAPI::class.java) // class.java is Kotlin's way of referencing a Java class object
    }

    fun getPhoto(callback: Callback<PhotoList>) {
        val call = service.getPhotos()
        call.enqueue(callback)
    }
}