package com.xyz.photoapp.api

import com.xyz.photoapp.models.PhotoList
import retrofit2.Call
import retrofit2.http.GET

interface PhotoAPI {

    @GET("?key=8837839-4e554deaa6a95f5e9aabc0fa7&q=nature&image_type=photo")
    fun getPhotos() : Call<PhotoList>
}