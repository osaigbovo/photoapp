package com.xyz.photoapp.models

// Data comes from the API as an array of Photos. Holds the List of Photos
data class PhotoList(val hits: List<Photo>) {
}