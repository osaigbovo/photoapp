package com.xyz.photoapp.models

import java.io.Serializable

// Holds the Photo information.
data class Photo(val id: String,
                 val likes: Int,
                 val favorites: Int,
                 val tags: String,
                 val previewURL: String,
                 val webformatURL: String) : Serializable {
}